function giveSourceID(creep: Creep) {
  let occupiedHarvestPoints = _.filter(Game.creeps, (creep) => creep.memory.role == 'dropminer').map((el) => el.memory.sourceid);
  let closestSource = creep.pos.findClosestByRange(FIND_SOURCES, {filter: (source) => occupiedHarvestPoints.indexOf(source.id) == -1});
  if (closestSource === null) {
    console.log("appears to be null");
  } else {
    creep.memory.sourceid = closestSource.id;
  }
}

export function run(creep: Creep) {
  // console.log("test");
  if (creep.memory.sourceid === "na") {
    giveSourceID(creep);
  } else {
    const sourceID = creep.memory.sourceid;
    const target: any = Game.getObjectById(creep.memory.sourceid);

    // console.log(sourceID);

    // console.log(target);
    if (target === null) {
      console.log("DropMiner ERROR - Something went wrong with the ID's");
    } else {
      if (creep.harvest(target) === ERR_NOT_IN_RANGE) {
        creep.moveTo(target);
      }
    }
  }
}
