import * as tasks from "logic/other/tasks";

export function run(creep: Creep) {
  if (creep.store.getFreeCapacity() === 0) {
    creep.memory.working = true;
  }
  if (creep.store.getUsedCapacity() === 0) {
    creep.memory.working = false;
  }
  if (!creep.memory.working) {
    // The Creep doesnt have any energy and needs to go and get some.
    tasks.getEnergy(creep);
  }

  const targets = creep.room.find(FIND_STRUCTURES, {
    filter: s =>
      s.hits < s.hitsMax && !(s.structureType === STRUCTURE_RAMPART || s.structureType === STRUCTURE_WALL)
  });
  targets.sort((a,b) => a.hits - b.hits)
  if (creep.memory.working) {
    if (targets.length) {
      // found something to repair
      if (creep.repair(targets[0]) === ERR_NOT_IN_RANGE) {
        creep.moveTo(targets[0]);
      }
    } else {
      // console.log("repairer test");
      // finding a wall or rampart to repair
      const wallRamparts = creep.room.find(FIND_STRUCTURES, {
        filter: s => s.hits < s.hitsMax && (s.structureType === STRUCTURE_RAMPART || s.structureType === STRUCTURE_WALL)
      });
      wallRamparts.sort((a, b) => a.hits - b.hits);
      if (wallRamparts.length) {
        if (creep.repair(wallRamparts[0]) === ERR_NOT_IN_RANGE) {
          creep.moveTo(wallRamparts[0]);
        }
      }
    }
  }
}
