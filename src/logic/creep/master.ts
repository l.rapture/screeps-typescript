import * as starter from "./starters";
import * as builder from "./builder";
import * as repaier from "./rapairer";
import * as dropminer from "./dropMiner";
import * as lorry from "./lorry";

export function runAll(creep: Creep) {
  if (creep.memory.role === "starter") {
    // Run the Starter Code
    starter.run(creep);
  }
  if (creep.memory.role === "builder") {
    // Run the builder Code
    builder.run(creep);
  }
  if (creep.memory.role === "repairer") {
    repaier.run(creep);
  }
  if (creep.memory.role === "dropminer") {
    dropminer.run(creep);
  }
  if (creep.memory.role === "lorry") {
    lorry.run(creep);
  }
}
