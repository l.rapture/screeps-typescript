import * as tasks from "../other/tasks";

function giveSourceID(creep: Creep) {
  let occupiedHarvestPoints = _.filter(Game.creeps, (creep) => creep.memory.role == 'lorry').map((el) => el.memory.sourceid);
  let closestSource = creep.pos.findClosestByRange(FIND_SOURCES, {filter: (source) => occupiedHarvestPoints.indexOf(source.id) == -1});
  if (closestSource === null) {
    console.log("appears to be null");
  } else {
    creep.memory.sourceid = closestSource.id;
  }
}

export function run(creep: Creep) {
  if (creep.memory.sourceid === "na") {
    giveSourceID(creep);
  }
  if (creep.store.getUsedCapacity() === 0) {
    creep.memory.working = false;
  }
  if (creep.store.getFreeCapacity() === 0) {
    creep.memory.working = true;
  }
  if (!creep.memory.working) {
    // The Creep has space to pick things up
    const target: any = Game.getObjectById(creep.memory.sourceid);
    // console.log(target);
    const sourcePickup: any = target.pos.findInRange(FIND_DROPPED_RESOURCES, 5);
    // console.log(sourcePickup);
    if (creep.pickup(sourcePickup[0]) === ERR_NOT_IN_RANGE) {
      creep.moveTo(sourcePickup[0]);
    }

  }
  if (creep.memory.working) {
    // The creep is full and is finding its way back to base
    // Checking for storage
    const storage = creep.pos.findClosestByPath(FIND_STRUCTURES, {
      filter: (s) => (s.structureType === STRUCTURE_STORAGE) &&
        s.store.getFreeCapacity(RESOURCE_ENERGY) > 0
    });
    if (storage) {
      // Not written yet
    } else {
      const target = creep.pos.findClosestByPath(FIND_STRUCTURES, {
        filter: (s) => (s.structureType === STRUCTURE_SPAWN || s.structureType === STRUCTURE_EXTENSION) &&
          s.store.getFreeCapacity(RESOURCE_ENERGY) > 0
      });
      if (target === null) {
        creep.say("Upgrading room");
        // console.log("STARTER: No Spawns to move to ... Upgrading room");
        tasks.upgrade(creep);
      } else {
        if (creep.transfer(target, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
          creep.moveTo(target);
        }
      }
    }
  }
}
