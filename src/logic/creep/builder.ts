import * as tasks from "logic/other/tasks";

export function run(creep: Creep) {
  // console.log("test builder");
  if (creep.store.getUsedCapacity() === 0) {
    creep.memory.working = false;
  }
  if (creep.store.getFreeCapacity() === 0) {
    creep.memory.working = true;
  }
  if (!creep.memory.working) {
    // The Creep doesnt have any energy and needs to go and get some.
    tasks.getEnergy(creep);
  }
  if (creep.memory.working) {
    // find all construction plots in the room
    const buildTargets = creep.room.find(FIND_CONSTRUCTION_SITES);
    if (buildTargets.length) {
      // There is a construction site to build
      if (creep.build(buildTargets[0]) === ERR_NOT_IN_RANGE) {
        creep.say("🚧 build");
        creep.moveTo(buildTargets[0]);
      }
    } else {
      // There are no constuction spots in the room. Looking for walls and ramparts
      const wallRamparts = creep.room.find(FIND_STRUCTURES, {
        filter: s => s.hits < s.hitsMax && (s.structureType === STRUCTURE_RAMPART || s.structureType === STRUCTURE_WALL)
      });
      wallRamparts.sort((a, b) => a.hits - b.hits);
      if (wallRamparts.length) {
        if (creep.repair(wallRamparts[0]) === ERR_NOT_IN_RANGE) {
          creep.moveTo(wallRamparts[0]);
        }
      }
    }
  }
}
