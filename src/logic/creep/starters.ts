import * as tasks from "logic/other/tasks";
import { getEnergy } from "logic/other/tasks";

export function run(creep: Creep) {
  if (creep.store.getUsedCapacity() === 0) {
    creep.memory.working = false;
  }
  if (creep.store.getFreeCapacity() === 0) {
    creep.memory.working = true;
  }
  if (!creep.memory.working) {
    // console.log("getting energy");
    // const sources = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE);
    // if (sources === null) {
    //   console.log("STARTER: ERROR: No Active Sources");
    // } else {
    //   if (creep.harvest(sources) === ERR_NOT_IN_RANGE) {
    //     creep.moveTo(sources);

    //   }
    // }
    getEnergy(creep);
  }
  if (creep.memory.working) {
    const target = creep.pos.findClosestByPath(FIND_STRUCTURES, {
      filter: (s) => (s.structureType === STRUCTURE_SPAWN || s.structureType === STRUCTURE_EXTENSION) &&
        s.store.getFreeCapacity(RESOURCE_ENERGY) > 0
    });
    if (target === null) {
      creep.say("Upgrading room");
      // console.log("STARTER: No Spawns to move to ... Upgrading room");
      tasks.upgrade(creep);
    } else {
      if (creep.transfer(target, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
        creep.moveTo(target);
      }
    }
  }
}
