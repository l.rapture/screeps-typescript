import * as spawnC from "../spawning/spawncreep";

const starterCount = 1;
const builderCount = 1;
const repairerCount = 2;
const dropMinerCount = 2;
const lorryCount = dropMinerCount;

export function searchCreeps(role: string) {
  const result = _.filter(Game.creeps, creep => creep.memory.role === role);
  return result;
}

export function spawnLogic(spawn: StructureSpawn) {
  // console.log(spawn);
  const roomLevel = spawn.room.controller?.level;
  if (roomLevel === undefined) {
    console.log("Couldnt find a controller");
  } else {
    if (roomLevel < 5) {
      // testing
      // spawnC.spawnCustomCreep("testicles", [WORK,CARRY,MOVE], spawn);
      // The Room level doesnt allow for building containers - Doing normal mining
      // Getting Creep numbers
      const numberOfStarters = searchCreeps("starter");
      const numberOfBuilders = searchCreeps("builder");
      const numberOfRepairers = searchCreeps("repairer");
      const numberOfDropMiners = searchCreeps("dropminer");
      const numberOfLorrys = searchCreeps("lorry");
      // Running spawning based on creep numbers
      if (!spawn.spawning) {
        if (numberOfLorrys.length < lorryCount) {
          spawnC.spawnCustomCreep("lorry", [WORK, CARRY, CARRY, CARRY, MOVE, MOVE], spawn);
        }
        if (numberOfDropMiners.length < dropMinerCount) {
          spawnC.spawnCustomCreep("dropminer", [WORK,WORK,WORK,WORK,WORK,MOVE], spawn);
        }
        if (numberOfBuilders.length < builderCount) {
          spawnC.spawnCustomCreep("builder", [WORK,CARRY,MOVE], spawn);
        }
        if (numberOfRepairers.length < repairerCount) {
          spawnC.spawnCustomCreep("repairer", [WORK,CARRY,MOVE], spawn);
        }
        if (numberOfStarters.length < starterCount) {
          spawnC.spawnCustomCreep("starter", [WORK,WORK,CARRY,MOVE], spawn);
        }
      }
    }
    if (roomLevel > 5) {
      console.log("oooppppps you havnt coded this yet!");
    }
  }
}
