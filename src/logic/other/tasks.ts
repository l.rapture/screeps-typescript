import * as tasks from "logic/room/master";
import { searchCreeps } from "logic/room/master";

export function upgrade(creep: Creep) {
  const contr = creep.room.controller;
  if (contr === undefined) {
    console.log("UPGRADING: ERROR: There appears to be no controller in the room");
  } else {
    // console.log(contr.level);
    if (creep.upgradeController(contr) === ERR_NOT_IN_RANGE) {
      creep.moveTo(contr, {visualizePathStyle: {stroke: '#ffffff'}});
    }
  }
}
export function getEnergy(creep: Creep) {
  // Finding if there is any storages in the same room
  const roomStorage = creep.room.find(FIND_STRUCTURES, {
    filter: (s) => (s.structureType === STRUCTURE_STORAGE) && s.store.getUsedCapacity() > 0
  });
  // console.log(roomStorage);
  if (roomStorage.length === 0) {
    //console.log("there is no storage in the room");
    if (searchCreeps("dropminer").length){
      // console.log("there be a drop miner");
      const sources = creep.pos.findClosestByPath(FIND_DROPPED_RESOURCES);
      if (sources === null) {
        console.log("STARTER: ERROR: No Active Sources");
      } else {
        if (creep.pickup(sources) === ERR_NOT_IN_RANGE) {
          creep.moveTo(sources);
        }
      }
    } else {
      const sources = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE);
      if (sources === null) {
        console.log("STARTER: ERROR: No Active Sources");
      } else {
        if (creep.harvest(sources) === ERR_NOT_IN_RANGE) {
          creep.moveTo(sources);
        }
      }
    }
  } else {
    console.log("there is storage in this room");
  }
}
