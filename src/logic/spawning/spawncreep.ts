export function spawnCustomCreep(givenRole: string, body: BodyPartConstant[], spawn: StructureSpawn) {
  // console.log(givenRole + " " + body + " " + spawn);
  if (spawn.spawning) {
    const spawningCreep = Game.creeps[spawn.spawning.name];
    console.log("Currently spawning: " + spawningCreep)
  } else {
    const newName = givenRole + Game.time;
    const roomName = spawn.room.name;
    // console.log("Spawning new " + givenRole + ": " + newName);
    spawn.spawnCreep(body, newName, {
      memory: { role: givenRole, working: false, room: roomName, sourceid: "na" }
    });
  }
}
