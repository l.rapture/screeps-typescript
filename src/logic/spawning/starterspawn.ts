export function spawnStarter(spawn: StructureSpawn) {
  if (spawn.spawning) {
    const spawningCreep = Game.creeps[spawn.spawning.name];
    console.log("Currently spawning: " + spawningCreep)
  } else {
    const newName = "starter" + Game.time;
    const roomName = spawn.room.name;
    console.log("Spawning new starter: " + newName);
    spawn.spawnCreep([WORK,CARRY,MOVE], newName,
      {memory: {role: 'starter', working: false, room: roomName}
    });
  }
}
