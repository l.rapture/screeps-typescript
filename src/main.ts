import { ErrorMapper } from "utils/ErrorMapper";
// eslint-disable-next-line sort-imports
import * as masterControl from "logic/creep/master";
import * as spawnMaster from "logic/room/master";


// When compiling TS to JS and bundling with rollup, the line numbers and file names in error messages change
// This utility uses source maps to get the line numbers and file names of the original, TS source code
export const loop = ErrorMapper.wrapLoop(() => {
  const timecheck = Game.time;
  if (timecheck % 5 === 0) {
    console.log(`Current game tick is ${Game.time}`);
  }

  // Automatically delete memory of missing creeps
  for (const name in Memory.creeps) {
    if (!(name in Game.creeps)) {
      delete Memory.creeps[name];
    }
  }

  for (const spawn in Game.spawns) {
    spawnMaster.spawnLogic(Game.spawns[spawn]);
  }

  for (const name in Game.creeps) {
    const creep = Game.creeps[name];
    masterControl.runAll(creep);
  }
});
